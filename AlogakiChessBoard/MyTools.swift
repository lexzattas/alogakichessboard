//
//  MyTools.swift
//  ChessGame
//
//  Created by Alexandros.N.Zattas on 05/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit


public final class Device {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
}
