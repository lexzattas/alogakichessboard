//
//  ChessScreenView.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 06/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit

/// Returns the magnitude of a vector in three dimensions
@objc protocol ButtonActionsProtocol {
    @objc func resetButtonAction()
    @objc func exitButtonAction()
}

/// Creates the view for the ChessBoardViewController
class ChessScreenView: UIView {
    
    var collectionView: ChessCollectionView?
    
    var tableView: ResultsListTableView?
    
    let startingPositionLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont(name: "IowanOldStyle-BoldItalic", size: 16)
        label.text = "Starting Position: "
        return label
    }()
    
    let finishingPositionLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont(name: "IowanOldStyle-BoldItalic", size: 16)
        label.text = "Finishing Position: "
        return label
    }()
    
    let resetButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.white
        button.setTitle("Reset", for: .normal)
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        return button
    }()
    
    let exitButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        button.setTitle("Exit", for: .normal)
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        return button
    }()
    
    let containerView : UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Device.width, height: Device.width))
        view.addShadow(15, 0.5)
        return view
    }()

    let backgroundImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "pantheon")
        imageView.alpha = 0.25
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    init(viewModel: ChessViewModel) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: Device.width, height: Device.height))
        
    
        
        self.backgroundColor = .white
        collectionView = ChessCollectionView(viewModel: viewModel)
        tableView = ResultsListTableView(delegate: viewModel.tableDelegate!)
        
        self.addElementsToSuperview(elements: [backgroundImageView, startingPositionLabel, finishingPositionLabel, containerView, resetButton, exitButton, tableView!])
        setUpConstraints()
        
        resetButton.addTarget((Any).self, action: #selector(viewModel.buttonDelegate?.resetButtonAction), for: .touchUpInside)
        exitButton.addTarget((Any).self, action: #selector(viewModel.buttonDelegate?.exitButtonAction), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setUpConstraints(){
        
        backgroundImageView.fillSuperview()

        startingPositionLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        startingPositionLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        startingPositionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        
        finishingPositionLabel.topAnchor.constraint(equalTo: startingPositionLabel.bottomAnchor, constant: 10).isActive = true
        finishingPositionLabel.leftAnchor.constraint(equalTo: startingPositionLabel.leftAnchor).isActive = true
        finishingPositionLabel.rightAnchor.constraint(equalTo: startingPositionLabel.rightAnchor).isActive = true
    
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.heightAnchor.constraint(equalToConstant: Device.width).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: Device.width).isActive = true
        containerView.topAnchor.constraint(equalTo: finishingPositionLabel.bottomAnchor, constant: 20).isActive = true
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        containerView.addShadow()
        
        if let thisCollectionView = self.collectionView {
            containerView.addSubview(thisCollectionView)
            thisCollectionView.translatesAutoresizingMaskIntoConstraints = false
            thisCollectionView.fillSuperview()
        }
        
        tableView?.topAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 10).isActive = true
        tableView?.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 10).isActive = true
        tableView?.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -10).isActive = true
        tableView?.bottomAnchor.constraint(equalTo: resetButton.topAnchor, constant: -15).isActive = true

        resetButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
        resetButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        resetButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        exitButton.topAnchor.constraint(equalTo: startingPositionLabel.topAnchor).isActive = true
        exitButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        exitButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        exitButton.bottomAnchor.constraint(equalTo: finishingPositionLabel.bottomAnchor).isActive = true
    }
}

/// Creates the chess board as a collection view
class ChessCollectionView : UICollectionView, UICollectionViewDataSource {
    
    // default size
    var boardDimensions: Int = 0
    var cellIdentifier = "ChessTileCollectionViewCell"
    var startingTile: IndexPath?
    var finishTile: IndexPath?
    var firstMove: IndexPath?
    var secondMove: IndexPath?
    var thirdMove: IndexPath?
    let tileNumbering = TileNumberingDataModel()


    init(viewModel: ChessViewModel){
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets.zero
        layout.collectionView?.contentMode = .bottomLeft

        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        self.delegate = viewModel.delegate
        self.dataSource = self
        self.register(ChessTileCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        self.boardDimensions = viewModel.boardSize
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: Device.width).isActive = true
        self.widthAnchor.constraint(equalToConstant: Device.width).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return boardDimensions
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return boardDimensions
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? ChessTileCollectionViewCell else { return UICollectionViewCell()}
        
        // Configure the cell
        cell.moveLabel.text = ""
        cell.tileNumberingLabel.text = ""

        cell.isUserInteractionEnabled = true

        if indexPath == firstMove {
            cell.moveLabel.text = "1"
        }
        if indexPath == secondMove {
            cell.moveLabel.text = "2"
        }
        if indexPath == thirdMove {
            cell.moveLabel.text = "3"
        }
      
        
        if indexPath == startingTile {
            cell.isUserInteractionEnabled = false
            cell.isSelected = true
            cell.selectedBackgroundView?.backgroundColor = UIColor.blue.withAlphaComponent(0.1)
        }
        else {
            cell.selectedBackgroundView?.backgroundColor = UIColor.clear
        }
        
        if indexPath.section % 2 == 0 {
            if indexPath.row % 2 == 0{
                cell.pathView.image = UIImage.init(named: "Black-Marble-Texture")
            }
            else {
                cell.pathView.image = UIImage.init(named: "White-Marble-Texture")
            }
        }
        else {
            if indexPath.row % 2 == 0 {
                cell.pathView.image = UIImage.init(named: "White-Marble-Texture")
            }
            else {
                cell.pathView.image = UIImage.init(named: "Black-Marble-Texture")
            }
        }
        
        
        if indexPath.row == 0 {
            cell.tileNumberingLabel.text = tileNumbering.getTileLetter(section: indexPath.section)
        }
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.tileNumberingLabel.text = "a1"
            }
            else {
                cell.tileNumberingLabel.text = String(indexPath.row + 1)
            }
        }
        return cell
    }
    
    
    func handleResults(results: [IndexPath]) {
        
        if results.isEmpty {
            print("No Results")
        }
        else {
            reloadDataWithResults(result: results)
        }
    }
    
    
    func reloadDataWithResults(result: [IndexPath]) {
        
            clearMoves()
            let moves = result.count - 1
            
            if moves == 1 {
                self.firstMove = result[moves]
            }
            else if moves == 2 {
                self.firstMove = result[moves - 1]
                self.secondMove = result[moves]
            }
            else if moves == 3 {
                self.firstMove = result[moves - 2]
                self.secondMove = result[moves - 1]
                self.thirdMove = result[moves]
            }
        self.reloadData()
    }
    
    func clearData(){
        clearMoves()
        self.startingTile = nil
        self.isUserInteractionEnabled = true
    }
    
    func clearMoves(){
        self.firstMove = nil
        self.secondMove = nil
        self.thirdMove = nil
    }
}



///  Creates the result table, in which the possible paths will be displayed
class ResultsListTableView: UITableView, UITableViewDataSource {
    
    
    var results: [[IndexPath]] = [] {
        didSet {
            self.reloadData()
        }
    }
    let tileNumbering = TileNumberingDataModel()

    
    convenience init(delegate: UITableViewDelegate) {
        self.init(frame: CGRect.zero)
        self.register(ResultTableViewCell.self, forCellReuseIdentifier: "ResultTableViewCell")
        self.register(NoResultsTableViewCell.self, forCellReuseIdentifier: "NoResultsTableViewCell")
        self.delegate = delegate
        self.dataSource = self
        self.separatorStyle = .none
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 6
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if results.isEmpty {
            return nil
        }
        else if results.count == 1 {
            return "Result: 1 possible path"
        }
        else {
            return "Result: \(results.count) possible paths"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if results.isEmpty {
            tableView.isScrollEnabled = false
            return 1
        }
        else {
            tableView.isScrollEnabled = true
            return results.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if results.isEmpty {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultsTableViewCell", for: indexPath) as? NoResultsTableViewCell  else {return UITableViewCell()}
            cell.titleLabel.text = "No Results"
            return cell
        }
        
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell", for: indexPath) as? ResultTableViewCell else {return UITableViewCell()}
        
            let result = self.results[indexPath.row]
            var positionText = ""
            for position in result {
                if position == result.last {
                    positionText =  positionText + tileNumbering.transformPositionNumbering(position: position, withArrow: false)
                }
                else {
                    positionText =  positionText + tileNumbering.transformPositionNumbering(position: position)
                }
            }
            cell.numberLabel.text = String(indexPath.row + 1) + "."
            cell.titleLabel.text = positionText
            return cell
    }
}
