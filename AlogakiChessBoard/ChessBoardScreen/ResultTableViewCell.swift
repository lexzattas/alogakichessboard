//
//  ResultTableViewCell.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 07/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {
    
    let numberLabel : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.addCellSubviews(elements: [numberLabel, titleLabel, separatorView])
        setUpConstraints()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.backgroundColor = .lightGray
        }
        else {
            self.backgroundColor = .clear
        }
    }
    
    private func setUpConstraints() {
        
        numberLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15).isActive = true
        numberLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        numberLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -9).isActive = true
        
        titleLabel.bottomAnchor.constraint(equalTo: numberLabel.bottomAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: numberLabel.rightAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10 ).isActive = true

        separatorView.leftAnchor.constraint(equalTo: titleLabel.leftAnchor, constant: -5 ).isActive = true
        separatorView.rightAnchor.constraint(equalTo: titleLabel.rightAnchor).isActive = true
        separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
}
