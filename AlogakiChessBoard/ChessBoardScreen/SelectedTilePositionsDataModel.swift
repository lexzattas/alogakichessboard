//
//  SelectedTilePositionsDataModel.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 06/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit


class SelectedTilePositionsDataModel {
    
    var startingPosition: IndexPath?
    var finishingPosition: IndexPath?
    var maxPosition: Int?

    init(startingPosition: IndexPath?, finishingPosition: IndexPath?, boardDimensions: Int){
        self.startingPosition = startingPosition
        self.finishingPosition = finishingPosition
        self.maxPosition = boardDimensions - 1
    }
}


class ChessViewModel {
    
    var buttonDelegate: ButtonActionsProtocol?
    var delegate: UICollectionViewDelegate?
    var tableDelegate: UITableViewDelegate?
    var boardSize: Int = 8
    
    init(buttonDelegate: ButtonActionsProtocol?, delegate: UICollectionViewDelegate?, boardSize: Int, tableDelegate: UITableViewDelegate){
        
        self.buttonDelegate = buttonDelegate
        self.delegate = delegate
        self.tableDelegate = tableDelegate
        self.boardSize = boardSize
    }
}


struct TileNumberingDataModel {
    
    let alphabetArray: [String] = ["a",
                                   "b",
                                   "c",
                                   "d",
                                   "e",
                                   "f",
                                   "g",
                                   "h",
                                   "i",
                                   "j",
                                   "k",
                                   "l",
                                   "m",
                                   "n",
                                   "o",
                                   "p"]
    
    
    func getTileLetter(section: Int) -> String {
        return alphabetArray[section]
    }
    
    func transformPositionNumbering(position: IndexPath, withArrow: Bool = true) -> String {
        
        var transformedPosition: String = getTileLetter(section: position.section)
        let row = String(position.row + 1)
        transformedPosition = transformedPosition + row
        if withArrow {
            transformedPosition = transformedPosition + " → "
        }
        return transformedPosition
    }
}
