//
//  TileMovements.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 05/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit


class TileMovements {
    
    var possibleTiles: [IndexPath] = []
    var secondaryTiles: [IndexPath] = []
    var successfullMoves: [[IndexPath]] = []
    var finishTile: IndexPath?
    var dataModel: SelectedTilePositionsDataModel?
    var maxTile: Int?

    
    init(dataModel: SelectedTilePositionsDataModel){
        self.finishTile = dataModel.finishingPosition
        self.dataModel = dataModel
        self.maxTile = self.dataModel?.maxPosition
        calculateMoves(currentTile: dataModel.startingPosition!)
    }
    
    
    func calculateMoves(currentTile: IndexPath){
        
        let firstMovementResultTiles = movement(previousTile: nil, currentTile: currentTile)
        
        if firstMovementResultTiles.isEmpty == false {
            for i in 0...firstMovementResultTiles.count - 1 {
                
                let secondMovementResultTiles = movement(previousTile: currentTile, currentTile: firstMovementResultTiles[i])
                let interTile = firstMovementResultTiles[i]
                
                for i in 0...secondMovementResultTiles.count - 1 {
                    _ = movement(previousTile: interTile, currentTile: secondMovementResultTiles[i])
                }
            }
        }
    }
    
    
    /// Will calculate all possible movements and assign those that are successful
    /// - Parameter previousTile: the position of the previous movement
    /// - Parameter currentTile: current position
    func movement(previousTile: IndexPath?, currentTile: IndexPath) -> [IndexPath] {
        
        // reseting possible tiles of following movements
        possibleTiles = []
        
        // (section) knight movement to the right
        checkRightMovement(currentTile: currentTile)
        // (section) knight movement to the left
        checkLeftMovement(currentTile: currentTile)
        
        return checkForSuccess(previousTile: previousTile, fromTile: currentTile)
    }
    
    
    /// After calculating any possible movements, will now check if any of them have reached the finishing tile
    func checkForSuccess(previousTile: IndexPath?, fromTile: IndexPath) -> [IndexPath] {
       
        var nextTiles: [IndexPath] = []
        
        for i in 0...possibleTiles.count - 1 {
            if possibleTiles[i] == self.finishTile {
                if fromTile == self.dataModel?.startingPosition{
                    successfullMoves.append([fromTile, self.finishTile!])
                    print("success")
                }
                else if previousTile == self.dataModel?.startingPosition{
                    successfullMoves.append([(self.dataModel?.startingPosition)!, fromTile, self.finishTile!])
                    print("success")
                }
                else if fromTile !=  self.dataModel?.startingPosition {
                    successfullMoves.append([(self.dataModel?.startingPosition)!, previousTile!, fromTile, self.finishTile!])
                    print("success")
                }
            }
            else {
                nextTiles.append(possibleTiles[i])
            }
        }
        successfullMoves = successfullMoves.removeDuplicates()
        nextTiles = nextTiles.removeDuplicates()
        
        return nextTiles
    }
    
    /// Will calculate all possible movements from the current tile position to the +X-axis
    func checkRightMovement(currentTile: IndexPath?){
        
        var nextTileRow: Int?
        var nextTileSection: Int?
        guard let currentRow = currentTile?.row else { return }
        guard let currentSection = currentTile?.section else { return }
        guard let maxTile = self.dataModel?.maxPosition else { return }

        
        if currentSection + 2 <= maxTile {
            nextTileSection = currentSection + 2
            if currentRow + 1  <= maxTile {
                nextTileRow = currentRow + 1
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
            if currentRow - 1  >= 0 {
                nextTileRow = currentRow - 1
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
        }
        if currentSection + 1 <= maxTile {
            nextTileSection = currentSection + 1
            if currentRow + 2  <= maxTile {
                nextTileRow = currentRow + 2
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
            if currentRow - 2  >= 0 {
                nextTileRow = currentRow - 2
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
        }
    }
    
    /// Will calculate all possible movements from the current tile position to the -X-axis
    func checkLeftMovement(currentTile: IndexPath?){
        
        var nextTileRow: Int?
        var nextTileSection: Int?
        guard let currentRow = currentTile?.row else { return }
        guard let currentSection = currentTile?.section else { return }
        guard let maxTile = self.dataModel?.maxPosition else { return }

        if currentSection - 2 >= 0 {
            nextTileSection = currentSection - 2
            if currentRow + 1  <= maxTile {
                nextTileRow = currentRow + 1
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
            if currentRow - 1  >= 0 {
                nextTileRow = currentRow - 1
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
        }
        if currentSection - 1 >= 0 {
            nextTileSection = currentSection - 1
            if currentRow + 2  <= maxTile {
                nextTileRow = currentRow + 2
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
            if currentRow - 2  >= 0 {
                nextTileRow = currentRow - 2
                appendTile(nextTileSection: nextTileSection, nextTileRow: nextTileRow)
            }
        }
    }
    
    func appendTile(nextTileSection: Int?, nextTileRow: Int?){
        
        guard let section = nextTileSection ,let row = nextTileRow else {return}
        possibleTiles.append(IndexPath.init(row: row, section: section))
    }
}
