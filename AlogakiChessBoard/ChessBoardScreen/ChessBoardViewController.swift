//
//  ChessBoardViewController.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 06/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit


/// Main screen of the game. Handles all user actions occuring on its screen such as selecting starting and finishing position
/// for the knight chess piece on the chess board. Selecting successfull movements from the Resuls Table View to be displayed
/// on the chess board. Reseting the status of the game. Exiting to the start screen.
class ChessBoardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,
UITableViewDelegate,  ButtonActionsProtocol {
    
    
    var boardSize : Int = 8
    var isSelected: Bool = false
    var startingTile: IndexPath? {
        didSet {
            
        }
    }
    var finishTile: IndexPath?
    var collectionView: ChessCollectionView?
    var chessScreenView: ChessScreenView?
    var successfullMoves: [[IndexPath]] = []
    let tileNumbering = TileNumberingDataModel()


    
    // Will instantiate the ChessViewModel class and assign it to this class's view
    override func loadView() {
        let viewModel = ChessViewModel(buttonDelegate: self, delegate: self, boardSize: boardSize, tableDelegate: self)
        let thisView = ChessScreenView(viewModel: viewModel)
        chessScreenView = thisView
        self.collectionView = thisView.collectionView
        self.view = chessScreenView
    }
    
    
    override func viewDidLoad() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // Handles chess board tile selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.startingTile == nil {
            self.startingTile = indexPath
            self.collectionView?.startingTile = indexPath
            chessScreenView?.startingPositionLabel.text = (chessScreenView?.startingPositionLabel.text ?? "") +
                tileNumbering.transformPositionNumbering(position: indexPath, withArrow: false)
            self.collectionView?.reloadItems(at: [indexPath])
        }
        else {
            self.finishTile = indexPath
            collectionView.isUserInteractionEnabled = false
            chessScreenView?.finishingPositionLabel.text = (chessScreenView?.finishingPositionLabel.text ?? "") +
                tileNumbering.transformPositionNumbering(position: indexPath, withArrow: false)
            makeMoves()
        }
    }
    
    // Handles selection for the Results Table View. If any, will display the knight piece's movements on the chessboard/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.collectionView?.handleResults(results: self.successfullMoves[indexPath.row])
    }

    /// After selecting the finishing position on the chess board this method will calculate all possible movements
    /// for the knight piece
    func makeMoves(){
        
        let dataModel = SelectedTilePositionsDataModel.init(startingPosition: startingTile, finishingPosition: finishTile, boardDimensions: boardSize)
        let moves = TileMovements(dataModel: dataModel)
        successfullMoves = moves.successfullMoves
        self.chessScreenView?.tableView?.results = successfullMoves
        if successfullMoves.isEmpty {
            showAlert()
        }
        print(moves.successfullMoves)
    }
    
    /// Action for the reset button. Resets all values to start over the game with selecting anew starting and finishing tile
    func resetButtonAction() {
        
        self.chessScreenView?.tableView?.results = []
        self.collectionView?.clearData()
        chessScreenView?.startingPositionLabel.text = "Starting Position: "
        chessScreenView?.finishingPositionLabel.text = "Finishing Position: "
        self.startingTile = nil
        self.finishTile = nil
        self.successfullMoves = []
        self.collectionView?.reloadData()
    }
    
    func exitButtonAction() {
        
        let alert = UIAlertController(title: "Do you wish to exit to the home screen?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}





// Extra methods which are not a result of a user action, but have to be implemented by this class
extension ChessBoardViewController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: Device.width/CGFloat(boardSize), height: Device.width/CGFloat(boardSize))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if successfullMoves.isEmpty {
            return (self.chessScreenView?.tableView?.frame.height)!
        }
        return 40
    }
    
    func showAlert(){
        
        let alert = UIAlertController(title: "No successful paths found", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Return", style: .default, handler: { action in
            self.resetButtonAction()
        }))
        self.present(alert, animated: true)
    }
}
