//
//  ChessTileCollectionViewCell.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 05/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit

class ChessTileCollectionViewCell: UICollectionViewCell {

    let pathView = UIImageView()
    
    let moveLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .gray
        label.font = .boldSystemFont(ofSize: 28)
        label.textAlignment = .center
        label.text = ""
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let tileNumberingLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = .boldSystemFont(ofSize: 10)
        label.textAlignment = .right
        label.addShadow()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundView = UIView()
        self.selectedBackgroundView = UIImageView.init(image: UIImage(named: "knight"))
        self.addSubview(pathView)
        self.addSubview(moveLabel)
        self.addSubview(tileNumberingLabel)
        pathView.fillSuperview()
        moveLabel.fillSuperview()
        tileNumberingLabel.anchor(top: nil, leading: nil, bottom: self.bottomAnchor, trailing: self.trailingAnchor,
                               padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 2))
        self.bringSubviewToFront(self.selectedBackgroundView!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
