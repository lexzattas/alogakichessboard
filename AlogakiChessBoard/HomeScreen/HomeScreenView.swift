//
//  HomeScreenView.swift
//  ChessBoardGame
//
//  Created by Alexandros.N.Zattas on 08/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit


/// Returns the magnitude of a vector in three dimensions
@objc protocol HomeButtonActionsProtocol {
    @objc func startButtonAction()
    @objc func keyboardDoneButtonAction()
}

/// Creates the view for the HomeScreenViewController
class HomeScreenView: UIView {
    
    let knightImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "home_chess_piece")
        imageView.contentMode = .scaleAspectFit
        imageView.alpha = 0.3
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.text = "alogaki"
        label.font = UIFont(name: "SnellRoundhand-Black", size: 60)
        return label
    }()
    
    let textFieldContainerView : UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Device.width - 40, height: Device.height/4))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.05)
        view.layer.cornerRadius = 10
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 0.25

        return view
    }()
    
    let descriptionLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.text = "Enter board dimension size"
        label.font = UIFont(name: "DamascusLight", size: 16)
        return label
    }()

    let textFieldView : UITextField = {
        let textField = UITextField()
        textField.borderStyle = .line
        textField.returnKeyType = .next
        textField.placeholder = "8"
        textField.textAlignment = .center
        textField.keyboardType = .numberPad
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.becomeFirstResponder()
        return textField
    }()

    let startButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 10
        button.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.8), forState: .normal)
        button.setBackgroundColor(color: .lightGray, forState: .disabled)
        button.setTitle("Start", for: .normal)
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        return button
    }()
    
    let errorLineLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .red
        label.textAlignment = .center
        label.text = "Board dimension size must be within the range of 6 and 16"
        label.font = UIFont(name: "DamascusLight", size: 12)
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    
    init(delegate: UITextFieldDelegate, buttonDelegate: HomeButtonActionsProtocol) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: Device.width, height: Device.height))
        
        self.backgroundColor = .white
        addElementsToSuperview(elements: [textFieldContainerView, knightImageView, titleLabel, errorLineLabel])
        textFieldContainerView.addElementsToSuperview(elements: [descriptionLabel, textFieldView, startButton])
        setupConstraints()
        addDoneButtonOnKeyboard(buttonDelegate: buttonDelegate)
        textFieldView.delegate = delegate
        startButton.addTarget((Any).self, action: #selector(buttonDelegate.startButtonAction), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        
        titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: textFieldContainerView.topAnchor, constant: 0).isActive = true

        knightImageView.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor).isActive = true
        knightImageView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        knightImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        knightImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        textFieldContainerView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -Device.height/8).isActive = true
        textFieldContainerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        textFieldContainerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        textFieldContainerView.heightAnchor.constraint(equalToConstant: textFieldContainerView.frame.height).isActive = true
        textFieldContainerView.addBottomShadow()
        
        descriptionLabel.topAnchor.constraint(equalTo: textFieldContainerView.topAnchor, constant: 10).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: textFieldView.topAnchor, constant: 0).isActive = true

        textFieldView.bottomAnchor.constraint(equalTo: startButton.topAnchor, constant: -10).isActive = true
        textFieldView.leftAnchor.constraint(equalTo: textFieldContainerView.leftAnchor, constant: Device.width/4).isActive = true
        textFieldView.rightAnchor.constraint(equalTo: textFieldContainerView.rightAnchor, constant: -Device.width/4).isActive = true
        textFieldView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        startButton.bottomAnchor.constraint(equalTo: textFieldContainerView.bottomAnchor).isActive = true
        startButton.leftAnchor.constraint(equalTo: textFieldContainerView.leftAnchor).isActive = true
        startButton.rightAnchor.constraint(equalTo: textFieldContainerView.rightAnchor).isActive = true
        startButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        errorLineLabel.topAnchor.constraint(equalTo: textFieldContainerView.bottomAnchor, constant: 20).isActive = true
        errorLineLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        errorLineLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        errorLineLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    
    func addDoneButtonOnKeyboard(buttonDelegate: HomeButtonActionsProtocol){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: (Any).self, action: #selector(buttonDelegate.keyboardDoneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textFieldView.inputAccessoryView = doneToolbar
    }
}

