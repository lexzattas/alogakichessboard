//
//  ViewController.swift
//  ChessGame
//
//  Created by Alexandros.N.Zattas on 05/08/2019.
//  Copyright © 2019 Alexandros.N.Zattas. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController, UITextFieldDelegate, HomeButtonActionsProtocol {
    
    var boardSize: Int?
    var homeScreenView: HomeScreenView?

    override func loadView() {
        homeScreenView = HomeScreenView.init(delegate: self, buttonDelegate: self)
        view = homeScreenView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let input = textField.text else {
            return false
        }
        boardSize = Int(input)
        return true
    }
    
    
    func startButtonAction() {
        
        homeScreenView?.textFieldView.resignFirstResponder()

        let boardSizeInput = boardSize ?? 8
        // if input is valid start the game
        if boardSizeInput >= 6,  boardSizeInput <= 16 {
            let vc = ChessBoardViewController()
            vc.boardSize = boardSizeInput
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // if input is invalid present error
        else {
            self.homeScreenView?.startButton.isEnabled = false
            self.homeScreenView?.errorLineLabel.isHidden = false
        }
    }

    func keyboardDoneButtonAction() {
        _ = textFieldShouldReturn((self.homeScreenView?.textFieldView)!)
        homeScreenView?.textFieldView.resignFirstResponder()
    }
}



extension HomeScreenViewController {
    
    
    //MARK:TEXTFIELD VALIDATOR
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.homeScreenView?.startButton.isEnabled = true
        self.homeScreenView?.errorLineLabel.isHidden = true

        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        boardSize = Int(updatedText)
        return updatedText.count <= 2
    }
}
